<?php

namespace App\Controller;

use App\Entity\GroupPerson;
use App\Form\GroupPersonType;
use App\Repository\GroupPersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/group/person")
 */
class GroupPersonController extends AbstractController
{
    /**
     * @Route("/", name="group_person_index", methods={"GET"})
     */
    public function index(GroupPersonRepository $groupPersonRepository): Response
    {
        return $this->render('group_person/index.html.twig', [
            'group_people' => $groupPersonRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="group_person_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $groupPerson = new GroupPerson();
        $form = $this->createForm(GroupPersonType::class, $groupPerson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($groupPerson);
            $entityManager->flush();

            return $this->redirectToRoute('group_person_index');
        }

        return $this->render('group_person/new.html.twig', [
            'group_person' => $groupPerson,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="group_person_show", methods={"GET"})
     */
    public function show(GroupPerson $groupPerson): Response
    {
        return $this->render('group_person/show.html.twig', [
            'group_person' => $groupPerson,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="group_person_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, GroupPerson $groupPerson): Response
    {
        $form = $this->createForm(GroupPersonType::class, $groupPerson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('group_person_index');
        }

        return $this->render('group_person/edit.html.twig', [
            'group_person' => $groupPerson,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="group_person_delete", methods={"DELETE"})
     */
    public function delete(Request $request, GroupPerson $groupPerson): Response
    {
        if ($this->isCsrfTokenValid('delete'.$groupPerson->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($groupPerson);
            $entityManager->flush();
        }

        return $this->redirectToRoute('group_person_index');
    }
}
