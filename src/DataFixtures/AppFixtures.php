<?php

namespace App\DataFixtures;

use Faker;
use DateTime;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    function randomDateInRange(DateTime $start, DateTime $end) {
        $randomTimestamp = mt_rand($start->getTimestamp(), $end->getTimestamp());
        $randomDate = new DateTime();
        $randomDate->setTimestamp($randomTimestamp);
        return $randomDate;
    }

    public function load(ObjectManager $manager)
    {
        // On configure dans quelles langues nous voulons nos données
        $faker = Faker\Factory::create('fr_FR');

        // on créé 10 personnes
        for ($i = 0; $i < 10; $i++) {
            $personne = new Person();
            $personne->setFirstname($faker->firstName);
            $personne->setLastname($faker->lastName);
            $datestart=new DateTime('1980-01-01');
            $dateend=new DateTime('2000-01-01');
            $personne->setBithday(self::randomDateInRange($datestart, $dateend));
            $manager->persist($personne);
        }
 

        $manager->flush();
    }
}
