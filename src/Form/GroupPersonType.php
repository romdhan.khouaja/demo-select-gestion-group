<?php

namespace App\Form;

use App\Entity\Person;
use App\Entity\GroupPerson;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class GroupPersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('persons', EntityType::class, [
                'class' => Person::class,
                'choice_label' => 'lastname',
                'multiple' => true,
                'required' => false,
                'label' => 'Choisisez personne(s)',
                'placeholder' => 'Choisir personne(s)',
                'attr' => array(
                    'class' => 'js-select'),

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GroupPerson::class,
        ]);
    }
}
